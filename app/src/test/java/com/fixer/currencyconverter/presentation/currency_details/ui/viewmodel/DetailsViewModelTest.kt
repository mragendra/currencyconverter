package com.fixer.currencyconverter.presentation.currency_details.ui.viewmodel

import com.fixer.currencyconverter.BaseViewModelTest
import com.fixer.currencyconverter.common.Constant
import com.fixer.currencyconverter.common.Resource
import com.fixer.currencyconverter.domain.model.LatestCurrencyRate
import com.fixer.currencyconverter.domain.usecase.GetHistoricalCurrencyRatesUsecase
import com.fixer.currencyconverter.domain.usecase.GetLatestCurrencyRatesUsecase
import com.google.common.truth.Truth
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class DetailsViewModelTest : BaseViewModelTest() {

    private lateinit var viewModel: DetailsViewModel

    @Mock
    private lateinit var historicalCurrencyRatesUseCase: GetHistoricalCurrencyRatesUsecase

    @Mock
    private lateinit var latestCurrencyRateUseCase: GetLatestCurrencyRatesUsecase

    @Before
    fun setup() {
        viewModel = DetailsViewModel(historicalCurrencyRatesUseCase, latestCurrencyRateUseCase)
    }

    @Test
    fun `given conversion results when getHistory should return success`() = runBlockingTest {
        val map = getDummyOptions()
        val flowCurrencies = flowOf(getDummyHistoryConversionData())
        //WHEN
        Mockito.doReturn(flowCurrencies).`when`(historicalCurrencyRatesUseCase).invoke(Constant.FIXER_API_KEY, map)
        viewModel.getHistoricalCurrencyRate(Constant.FIXER_API_KEY, map)
        val currenciesList = viewModel.currencyRateList.value
        //THEN
        Truth.assertThat(currenciesList.data).isNotNull()

    }

    @Test
    fun `given conversion results when getLatestCurrencyRates should return success`() = runBlockingTest {
        val map = getDummyOptions()
        val flowCurrencies = flowOf(getDummyHistoryConversionData())
        Mockito.doReturn(flowCurrencies).`when`(latestCurrencyRateUseCase).invoke(map)
        viewModel.getLatestCurrencyRates(map)
        val currenciesList = viewModel.latestCurrencyRateList.value
        Truth.assertThat(currenciesList.data).isNotNull()
    }

    private fun getDummyHistoryConversionData(): Resource<LatestCurrencyRate> {
        var map = HashMap<String, String>()
        map["INR"] = ""
        map["EUR"] = ""
        map["USD"] = ""
        return Resource.Success(data = LatestCurrencyRate(map))
    }

    private fun getDummyOptions(): HashMap<String, String>{
        val option = HashMap<String, String>()
        option["access_key"] = Constant.FIXER_API_KEY
        option["base"] = Constant.BASE_CURRENCY
        option["symbols"] = "${"EUR"},${"INR"}"
        return option
    }

}