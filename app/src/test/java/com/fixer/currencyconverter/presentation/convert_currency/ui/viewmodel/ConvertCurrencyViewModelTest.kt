package com.fixer.currencyconverter.presentation.convert_currency.ui.viewmodel

import android.graphics.Insets.add
import com.fixer.currencyconverter.BaseViewModelTest
import com.fixer.currencyconverter.common.Constant
import com.fixer.currencyconverter.common.Resource
import com.fixer.currencyconverter.data.model.Rates
import com.fixer.currencyconverter.domain.model.LatestCurrencyRate
import com.fixer.currencyconverter.domain.usecase.CurrencyListUsecase
import com.fixer.currencyconverter.domain.usecase.GetLatestCurrencyRatesUsecase
import com.google.common.truth.Truth
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class ConvertCurrencyViewModelTest : BaseViewModelTest() {

    private lateinit var viewModel: ConvertCurrencyViewModel

    @Mock
    private lateinit var currencyListUseCase: CurrencyListUsecase

    @Mock
    private lateinit var latestCurrencyRateUseCase: GetLatestCurrencyRatesUsecase

    @Before
    fun setup() {
        viewModel = ConvertCurrencyViewModel(currencyListUseCase, latestCurrencyRateUseCase)
    }

    @Test
    fun `given currencies when getCurrencyList should return success`() = runBlockingTest {
        //GIVEN
        val flowCurrencies = flowOf(getDummyCurrency())
        //WHEN
        Mockito.doReturn(flowCurrencies).`when`(currencyListUseCase).invoke(Constant.FIXER_API_KEY)
        viewModel.getCurrencyList(Constant.FIXER_API_KEY)
        val currenciesList = viewModel.currencyList.value
        //THEN
        Truth.assertThat(currenciesList.data).isNotEmpty()
    }

    @Test
    fun `given currencies when getLatestCurrencyRates should return success`() = runBlockingTest {
        var option = viewModel.queryMap
        val flowCurrencies = flowOf(getDummyLatestCurrency())
        Mockito.doReturn(flowCurrencies).`when`(latestCurrencyRateUseCase).invoke(option)
        viewModel.getLatestCurrencyRates(option)
        val currenciesList = viewModel.currencyRateList.value
        //THEN
        Truth.assertThat(currenciesList.data).isNotNull()
    }


    private fun getDummyCurrency(): Resource<MutableList<String>> {
        return Resource.Success(data = mutableListOf<String>().apply {
            add("INR")
            add("EUR")
            add("USD")
        }
        )
    }

    private fun getDummyLatestCurrency(): Resource<LatestCurrencyRate> {
        var map = HashMap<String, String>()
        map["INR"] = ""
        map["EUR"] = ""
        map["USD"] = ""
        return  Resource.Success(data = LatestCurrencyRate(map))
    }

}