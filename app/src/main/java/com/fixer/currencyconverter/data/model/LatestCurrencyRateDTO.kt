package com.fixer.currencyconverter.data.model

import com.fixer.currencyconverter.domain.model.HistoricalCurrencyRate
import com.fixer.currencyconverter.domain.model.LatestCurrencyRate

data class LatestCurrencyRateDTO(
    var base: String?,
    var date: String?,
    var rates: HashMap<String, String>?,
    var success: Boolean?,
    var timestamp: Int?,
    var historical: Boolean?
)

fun LatestCurrencyRateDTO.toDomainLatestCurrencyRate(): LatestCurrencyRate {
    return LatestCurrencyRate(this.rates!!)
}

fun LatestCurrencyRateDTO.toDomainHistoricalCurrencyRate(): LatestCurrencyRate {
    return LatestCurrencyRate(rates = this.rates!!)
}