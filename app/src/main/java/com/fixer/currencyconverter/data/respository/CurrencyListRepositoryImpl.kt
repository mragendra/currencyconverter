package com.fixer.currencyconverter.data.respository

import com.fixer.currencyconverter.data.model.CurrencyDTO
import com.fixer.currencyconverter.data.remote.CurrencyApi
import com.fixer.currencyconverter.domain.repository.CurrencyListRepository

class CurrencyListRepositoryImpl(private val currencyApi: CurrencyApi): CurrencyListRepository {
    override suspend fun getCurrencyList(query: String): CurrencyDTO {
        return currencyApi.getCurrencyList(query)
    }
}