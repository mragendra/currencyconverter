package com.fixer.currencyconverter.data.remote

import com.fixer.currencyconverter.data.model.CurrencyDTO
import com.fixer.currencyconverter.data.model.LatestCurrencyRateDTO
import com.fixer.currencyconverter.data.model.Rates
import com.fixer.currencyconverter.data.model.Symbols
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.QueryMap

interface CurrencyApi {

    @GET("symbols")
    suspend fun getCurrencyList(@Query("access_key") query: String): CurrencyDTO

    @GET("latest")
    suspend fun getLatestCurrencyRate(@QueryMap option: Map<String, String>): LatestCurrencyRateDTO

    @GET("{date}")
    suspend fun getHistoricalCurrencyRates(@Path("date") date:String, @QueryMap option: Map<String, String>):  LatestCurrencyRateDTO
}