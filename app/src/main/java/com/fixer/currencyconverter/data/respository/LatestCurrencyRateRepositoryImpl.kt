package com.fixer.currencyconverter.data.respository

import com.fixer.currencyconverter.data.model.LatestCurrencyRateDTO
import com.fixer.currencyconverter.data.model.Rates
import com.fixer.currencyconverter.data.remote.CurrencyApi
import com.fixer.currencyconverter.domain.repository.LatestCurrencyRateRepository

class LatestCurrencyRateRepositoryImpl(private val currencyApi: CurrencyApi): LatestCurrencyRateRepository {
    override suspend fun getLatestCurrencyRate(option: Map<String, String>): LatestCurrencyRateDTO {
        return currencyApi.getLatestCurrencyRate(option)
    }
}