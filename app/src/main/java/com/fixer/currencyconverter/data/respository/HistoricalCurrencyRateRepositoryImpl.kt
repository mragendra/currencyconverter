package com.fixer.currencyconverter.data.respository

import com.fixer.currencyconverter.data.model.LatestCurrencyRateDTO
import com.fixer.currencyconverter.data.model.Rates
import com.fixer.currencyconverter.data.remote.CurrencyApi
import com.fixer.currencyconverter.domain.repository.HistoricalCurrencyRateRepository

class HistoricalCurrencyRateRepositoryImpl(private val currencyApi: CurrencyApi): HistoricalCurrencyRateRepository {
    override suspend fun getHistoricalCurrencyRate(date: String, option: Map<String, String>): LatestCurrencyRateDTO {
        return currencyApi.getHistoricalCurrencyRates(date, option)
    }
}