package com.fixer.currencyconverter.data.model

import com.fixer.currencyconverter.domain.model.Currency
import com.google.gson.Gson
import org.json.JSONObject


data class CurrencyDTO(
    val success: Boolean?,
    val symbols: Symbols?
)

fun CurrencyDTO.toDomainCurrency(): Currency {
    var symbolList: MutableList<String>? = null
    val baseJson = JSONObject(Gson().toJson(this.symbols))
    val iterator = baseJson.keys()
    while (iterator.hasNext()) {
        val key = iterator.next()
        symbolList!!.add(key)
    }
    return Currency(currencyList = symbolList!!)
}

fun CurrencyDTO.getCurrencyList(): Currency {
    var symbolList: MutableList<String> = mutableListOf()
    val baseJson = JSONObject(Gson().toJson(this.symbols))
    val iterator = baseJson.keys()
    while (iterator.hasNext()) {
        val key = iterator.next()
        symbolList.add(key)
    }
    return Currency(currencyList = symbolList)
}