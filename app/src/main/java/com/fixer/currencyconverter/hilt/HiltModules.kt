package com.fixer.currencyconverter.hilt

import com.fixer.currencyconverter.common.Constant.BASE_URL
import com.fixer.currencyconverter.data.remote.CurrencyApi
import com.fixer.currencyconverter.data.respository.CurrencyListRepositoryImpl
import com.fixer.currencyconverter.data.respository.HistoricalCurrencyRateRepositoryImpl
import com.fixer.currencyconverter.data.respository.LatestCurrencyRateRepositoryImpl
import com.fixer.currencyconverter.domain.repository.CurrencyListRepository
import com.fixer.currencyconverter.domain.repository.HistoricalCurrencyRateRepository
import com.fixer.currencyconverter.domain.repository.LatestCurrencyRateRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object HiltModules {

    @Provides
    @Singleton
    fun provideCurrencyApi(): CurrencyApi {
        return Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create()).build()
            .create(CurrencyApi::class.java)
    }

    @Provides
    fun provideCurrencyListRepository(currencyApi: CurrencyApi): CurrencyListRepository {
        return CurrencyListRepositoryImpl(currencyApi)
    }


    @Provides
    fun provideHistoricalCurrencyRateRepository(currencyApi: CurrencyApi): HistoricalCurrencyRateRepository {
        return HistoricalCurrencyRateRepositoryImpl(currencyApi)
    }

    @Provides
    fun provideLatestCurrencyRateRepository(currencyApi: CurrencyApi): LatestCurrencyRateRepository {
        return LatestCurrencyRateRepositoryImpl(currencyApi)
    }

}