package com.fixer.currencyconverter.domain.model

import com.fixer.currencyconverter.data.model.Rates

data class LatestCurrencyRate(
//    var rates: Rates
    var rates: HashMap<String, String>
)
