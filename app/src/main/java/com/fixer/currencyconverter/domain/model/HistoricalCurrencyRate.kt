package com.fixer.currencyconverter.domain.model

import com.fixer.currencyconverter.data.model.Rates

data class HistoricalCurrencyRate(
    var historical: Boolean,
    var rates: Rates
)
