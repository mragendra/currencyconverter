package com.fixer.currencyconverter.domain.repository

import com.fixer.currencyconverter.data.model.LatestCurrencyRateDTO

interface LatestCurrencyRateRepository {
    suspend fun getLatestCurrencyRate(option: Map<String, String>): LatestCurrencyRateDTO
}