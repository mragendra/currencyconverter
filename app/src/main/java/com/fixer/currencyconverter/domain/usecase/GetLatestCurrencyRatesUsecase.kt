package com.fixer.currencyconverter.domain.usecase

import com.fixer.currencyconverter.common.Resource
import com.fixer.currencyconverter.data.model.toDomainHistoricalCurrencyRate
import com.fixer.currencyconverter.data.model.toDomainLatestCurrencyRate
import com.fixer.currencyconverter.domain.model.HistoricalCurrencyRate
import com.fixer.currencyconverter.domain.model.LatestCurrencyRate
import com.fixer.currencyconverter.domain.repository.HistoricalCurrencyRateRepository
import com.fixer.currencyconverter.domain.repository.LatestCurrencyRateRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

open class GetLatestCurrencyRatesUsecase @Inject constructor(private val repository: LatestCurrencyRateRepository) {

    open operator fun invoke(option: Map<String, String>): Flow<Resource<LatestCurrencyRate?>> = flow {
        try {
            emit(Resource.Loading())
            val data = repository.getLatestCurrencyRate(option)
            val domainData = if (data != null) data.toDomainLatestCurrencyRate() else null
            emit(Resource.Success(data = domainData))
        } catch (e: HttpException) {
            emit(Resource.Error(message = e.localizedMessage ?: "An Unknown error occurred"))
        } catch (e: IOException) {
            emit(Resource.Error(message = e.localizedMessage ?: "Check Connectivity"))
        } catch (e: Exception) {
            emit(Resource.Error(message = e.localizedMessage ?: "Something went wrong, try again"))
        }
    }

}