package com.fixer.currencyconverter.domain.model

data class Currency(
  val currencyList: MutableList<String>
)
fun Currency.toDomainCurrency(): MutableList<String> {
  return this.currencyList
}
