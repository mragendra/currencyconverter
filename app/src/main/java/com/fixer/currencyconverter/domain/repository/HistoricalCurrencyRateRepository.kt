package com.fixer.currencyconverter.domain.repository

import com.fixer.currencyconverter.data.model.LatestCurrencyRateDTO

interface HistoricalCurrencyRateRepository {
    suspend fun getHistoricalCurrencyRate(date:String, option: Map<String, String>): LatestCurrencyRateDTO
}