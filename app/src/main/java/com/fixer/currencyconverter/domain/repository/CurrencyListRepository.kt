package com.fixer.currencyconverter.domain.repository

import com.fixer.currencyconverter.data.model.CurrencyDTO

interface CurrencyListRepository {

    suspend fun getCurrencyList(query: String): CurrencyDTO
}