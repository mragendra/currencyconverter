package com.fixer.currencyconverter.domain.usecase

import com.fixer.currencyconverter.common.Resource
import com.fixer.currencyconverter.data.model.toDomainHistoricalCurrencyRate
import com.fixer.currencyconverter.domain.model.LatestCurrencyRate
import com.fixer.currencyconverter.domain.repository.HistoricalCurrencyRateRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

open class GetHistoricalCurrencyRatesUsecase @Inject constructor(private val repository: HistoricalCurrencyRateRepository) {

    open operator fun invoke(date: String, option: Map<String, String>): Flow<Resource<LatestCurrencyRate?>> = flow {
        try {
            emit(Resource.Loading())
            val data = repository.getHistoricalCurrencyRate(date, option)
            val domainData = if (data != null) data.toDomainHistoricalCurrencyRate() else null
            emit(Resource.Success(data = domainData))
        } catch (e: HttpException) {
            emit(Resource.Error(message = e.localizedMessage ?: "An Unknown error occurred"))
        } catch (e: IOException) {
            emit(Resource.Error(message = e.localizedMessage ?: "Check Connectivity"))
        } catch (e: Exception) {
            emit(Resource.Error(message = e.localizedMessage ?: "Something went wrong"))
        }
    }

}