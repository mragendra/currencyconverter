package com.fixer.currencyconverter.domain.model

data class CurrencyDetail(
    var date: String?,
    var currency1Value: String?,
    var currency2Value: String?,
)
