package com.fixer.currencyconverter.domain.usecase

import com.fixer.currencyconverter.common.Resource
import com.fixer.currencyconverter.data.model.getCurrencyList
import com.fixer.currencyconverter.domain.model.toDomainCurrency
import com.fixer.currencyconverter.domain.repository.CurrencyListRepository
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

open class CurrencyListUsecase @Inject constructor(private val repository: CurrencyListRepository) {

    open operator fun invoke(q: String): kotlinx.coroutines.flow.Flow<Resource<MutableList<String>>> = flow {
        try {
            emit(Resource.Loading())
            val data = repository.getCurrencyList(q).getCurrencyList()
            val domainData =
                if (data.currencyList != null) data.toDomainCurrency() else mutableListOf()
            emit(Resource.Success(data = domainData))
        } catch (e: HttpException) {
            emit(Resource.Error(message = e.localizedMessage ?: "An Unknown error occurred"))
        } catch (e: IOException) {
            emit(Resource.Error(message = e.localizedMessage ?: "Check Connectivity"))
        } catch (e: Exception) {
            emit(Resource.Error(message = e.localizedMessage ?: "Something Went Wrong...try again"))
        }
    }
}