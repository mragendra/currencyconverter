package com.fixer.currencyconverter.presentation.currency_details.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fixer.currencyconverter.common.Resource
import com.fixer.currencyconverter.domain.usecase.GetHistoricalCurrencyRatesUsecase
import com.fixer.currencyconverter.domain.usecase.GetLatestCurrencyRatesUsecase
import com.fixer.currencyconverter.presentation.convert_currency.state.ConvertCurrencyState
import com.fixer.currencyconverter.presentation.convert_currency.state.GetLatestCurrencyRatesState
import com.fixer.currencyconverter.presentation.currency_details.DetailState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class DetailsViewModel @Inject constructor(private val historicalCurrencyRatesUsecase: GetHistoricalCurrencyRatesUsecase,
private val latestCurrencyRatesUsecase: GetLatestCurrencyRatesUsecase) : ViewModel() {

    private val _currencyRateList = MutableStateFlow(DetailState())
    val currencyRateList: StateFlow<DetailState> = _currencyRateList

    private val _latestCurrencyRateList = MutableStateFlow(GetLatestCurrencyRatesState())
    val latestCurrencyRateList: StateFlow<GetLatestCurrencyRatesState> = _latestCurrencyRateList

    fun getHistoricalCurrencyRate(date: String, map: HashMap<String, String>) {
        historicalCurrencyRatesUsecase(date, map).onEach {
            when (it) {
                is Resource.Loading -> {
                    _currencyRateList.value = DetailState(isLoading = true)
                }
                is Resource.Success -> {
                    _currencyRateList.value = DetailState(data = it.data)
                }
                is Resource.Error -> {
                    _currencyRateList.value = DetailState(error = it.message ?: "")
                }
            }
        }.launchIn(viewModelScope)
    }

    fun getLatestCurrencyRates(option: Map<String, String>) {
        latestCurrencyRatesUsecase.invoke(option).onEach {
            when (it) {
                is Resource.Loading -> {
                    _latestCurrencyRateList.value = GetLatestCurrencyRatesState(isLoading = true)
                }
                is Resource.Success -> {
                    _latestCurrencyRateList.value = GetLatestCurrencyRatesState(data = it.data)
                }
                is Resource.Error -> {
                    _latestCurrencyRateList.value = GetLatestCurrencyRatesState(error = it.message ?: "")
                }
            }
        }.launchIn(viewModelScope)
    }

}