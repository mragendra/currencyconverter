package com.fixer.currencyconverter.presentation.convert_currency.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fixer.currencyconverter.common.Resource
import com.fixer.currencyconverter.domain.usecase.CurrencyListUsecase
import com.fixer.currencyconverter.domain.usecase.GetLatestCurrencyRatesUsecase
import com.fixer.currencyconverter.presentation.convert_currency.state.ConvertCurrencyState
import com.fixer.currencyconverter.presentation.convert_currency.state.GetLatestCurrencyRatesState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class ConvertCurrencyViewModel @Inject constructor(private val currencyListUsecase: CurrencyListUsecase, private val latestCurrencyRatesUsecase: GetLatestCurrencyRatesUsecase) : ViewModel() {

    var queryMap: Map<String, String> = HashMap()
    private val _currencyList = MutableStateFlow(ConvertCurrencyState())
    val currencyList: StateFlow<ConvertCurrencyState> = _currencyList

    private val _currencyRateList = MutableStateFlow(GetLatestCurrencyRatesState())
    val currencyRateList: StateFlow<GetLatestCurrencyRatesState> = _currencyRateList


    fun getCurrencyList(s: String) {
        currencyListUsecase(s).onEach {
            when (it) {
                is Resource.Loading -> {
                    _currencyList.value = ConvertCurrencyState(isLoading = true)
                }
                is Resource.Success -> {
                    _currencyList.value = ConvertCurrencyState(data = it.data)
                }
                is Resource.Error -> {
                    _currencyList.value = ConvertCurrencyState(error = it.message ?: "")
                }
            }
        }.launchIn(viewModelScope)
    }

    fun getLatestCurrencyRates(option: Map<String, String>) {
        queryMap = option
        latestCurrencyRatesUsecase.invoke(option).onEach {
            when (it) {
                is Resource.Loading -> {
                    _currencyRateList.value = GetLatestCurrencyRatesState(isLoading = true)
                }
                is Resource.Success -> {
                    _currencyRateList.value = GetLatestCurrencyRatesState(data = it.data)
                }
                is Resource.Error -> {
                    _currencyRateList.value = GetLatestCurrencyRatesState(error = it.message ?: "")
                }
            }
        }.launchIn(viewModelScope)
    }
}