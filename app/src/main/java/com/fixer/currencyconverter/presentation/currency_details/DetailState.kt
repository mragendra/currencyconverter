package com.fixer.currencyconverter.presentation.currency_details

import com.fixer.currencyconverter.domain.model.LatestCurrencyRate

data class DetailState(
    val isLoading: Boolean = false,
    val data: LatestCurrencyRate? = null,
    val error: String = ""
)
