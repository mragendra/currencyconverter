package com.fixer.currencyconverter.presentation.convert_currency.ui.fragment

import android.R
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.viewModels
import androidx.lifecycle.coroutineScope
import com.fixer.currencyconverter.common.Constant
import com.fixer.currencyconverter.presentation.convert_currency.ui.viewmodel.ConvertCurrencyViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import android.view.inputmethod.EditorInfo
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.fixer.currencyconverter.databinding.FragmentConvertCurrencyBinding
import com.fixer.currencyconverter.domain.model.LatestCurrencyRate
import com.google.gson.Gson
import org.json.JSONObject


@AndroidEntryPoint
class ConvertCurrencyFragment : Fragment(), AdapterView.OnItemSelectedListener {

    private val viewModel: ConvertCurrencyViewModel by viewModels()
    private var _binding: FragmentConvertCurrencyBinding? = null
    private var currencyList = mutableListOf<String>()
    private lateinit var fromCurrency: String
    private lateinit var toCurrency: String
    private var isAllowConversion: Boolean = false
    private val defaultFromValue = "EGP"
    private val defaultToValue = "INR"
    private lateinit var latestCurrencyRate:LatestCurrencyRate
    val binding: FragmentConvertCurrencyBinding
        get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentConvertCurrencyBinding.inflate(inflater, container, false)
        return _binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.fromAmountEdit.setOnEditorActionListener { textview, actionId, event ->
            if (event != null && event.keyCode === KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
               currencyConversion()
                true
            }
            false
        }

        binding.toAmountEdit.setOnEditorActionListener { textview, actionId, event ->
            if (event != null && event.keyCode === KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                currencyAmountToQuantity()
                true
            }
            false
        }

        binding.swapBtn.setOnClickListener {
            swapCurrency()
        }

        binding.gotoDetailsBtn.setOnClickListener {
            openDetailFragment()
        }

        viewModel.getCurrencyList(Constant.FIXER_API_KEY)

        lifecycle.coroutineScope.launchWhenCreated {
            viewModel.currencyList.collect {
                if (it.isLoading) {
                    binding.nothingFound.visibility = View.GONE
                    binding.progressSearch.visibility = View.VISIBLE
                }
                if (it.error.isNotBlank()) {
                    binding.nothingFound.visibility = View.GONE
                    binding.progressSearch.visibility = View.GONE
                    Toast.makeText(requireContext(), it.error, Toast.LENGTH_SHORT).show()
                }

                it.data?.let {

                    if (it.isEmpty()) {
                        binding.nothingFound.visibility = View.VISIBLE
                    }
                    binding.progressSearch.visibility = View.GONE
                    setSpinnerAdapter(it.toMutableList())
                }

            }

            viewModel.currencyRateList.collect {
                if (it.isLoading) {
                    binding.nothingFound.visibility = View.GONE
                    binding.progressSearch.visibility = View.VISIBLE
                }
                if (it.error.isNotBlank()) {
                    binding.nothingFound.visibility = View.GONE
                    binding.progressSearch.visibility = View.GONE
                    Toast.makeText(requireContext(), it.error, Toast.LENGTH_SHORT).show()
                }

                it.data?.let {

                    if (it == null) {
                        binding.nothingFound.visibility = View.VISIBLE
                    }
                    binding.progressSearch.visibility = View.GONE
                    Toast.makeText(requireContext(), it.toString(), Toast.LENGTH_SHORT).show()
                }
            }
        }

    }

    private fun openDetailFragment() {
        findNavController().navigate(
            ConvertCurrencyFragmentDirections.actionConvertCurrencyFragemtToDetailsFragment(fromCurrency, toCurrency)
        )
    }

    private fun currencyAmountToQuantity() {
        if (latestCurrencyRate == null) {
            return
        }
        val jsonString = Gson().toJson(latestCurrencyRate.rates)
        var jsonObject = JSONObject(jsonString)
        val fromCurrencyRate = jsonObject.getString(fromCurrency).toDouble()
        val toCurrencyRate = jsonObject.getString(toCurrency).toDouble()
        var toAmountVal = binding.toAmountEdit.text.toString().toDouble()
        val quantity = (toAmountVal/toCurrencyRate) * fromCurrencyRate
        binding.fromAmountEdit.text = Editable.Factory.getInstance().newEditable(quantity.toString())
    }

    private fun callLatestCurrencyRateApi() {
        var option: MutableMap<String, String> = HashMap()
        option["access_key"] = Constant.FIXER_API_KEY
        option["base"] = Constant.BASE_CURRENCY
        option["symbols"] = getFromAndToValue()

        viewModel.getLatestCurrencyRates(option)
    }

    private fun getFromAndToValue(): String {
        val fromValue = binding.fromSpinner.selectedItem.toString()
        val toValue = binding.toSpinner.selectedItem.toString()
        return "$fromValue,$toValue"
    }

    private fun setSpinnerAdapter(list: MutableList<String>) {
        currencyList = list
        val arrayAdapter = ArrayAdapter(requireContext(), R.layout.simple_spinner_item, list)
        arrayAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
        binding.fromSpinner.adapter = arrayAdapter
        binding.toSpinner.adapter = arrayAdapter

        binding.fromSpinner.setSelection(list.indexOf(defaultFromValue))
        binding.toSpinner.setSelection(list.indexOf(defaultToValue))

        binding.toSpinner.onItemSelectedListener = this
        isAllowConversion = true
        binding.fromSpinner.onItemSelectedListener = this
    }

    private fun currencyConversion() {
        if (latestCurrencyRate == null) {
            return
        }
        val jsonString = Gson().toJson(latestCurrencyRate.rates)
        var jsonObject = JSONObject(jsonString)
        val fromCurrencyRate = jsonObject.getString(fromCurrency).toDouble()
        val toCurrencyRate = jsonObject.getString(toCurrency).toDouble()

        var oneOfFromCurrency = 1 / fromCurrencyRate
        var oneOfToCurrency = oneOfFromCurrency * toCurrencyRate
        var fromEditVal = binding.fromAmountEdit.text.toString().trim()
        var fromEditTextValue = if (!TextUtils.isEmpty(fromEditVal)) fromEditVal.toDouble() else 1.toDouble()
        var toAmount = fromEditTextValue * oneOfToCurrency
        binding.toAmountEdit.text =
            Editable.Factory.getInstance().newEditable(toAmount.toString())
    }

    private fun swapCurrency() {
        isAllowConversion = false
        var tempFromCurrency = fromCurrency
        binding.fromSpinner.setSelection(currencyList.indexOf(toCurrency))
        isAllowConversion = true
        binding.toSpinner.setSelection(currencyList.indexOf(tempFromCurrency))
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        if (!isAllowConversion) return
        fromCurrency = binding.fromSpinner.selectedItem.toString()
        toCurrency = binding.toSpinner.selectedItem.toString()
        callLatestCurrencyRateApi()
        lifecycle.coroutineScope.launchWhenCreated {
            viewModel.currencyRateList.collect {
                if (it.isLoading) {
                    binding.nothingFound.visibility = View.GONE
                    binding.progressSearch.visibility = View.VISIBLE
                }
                if (it.error.isNotBlank()) {
                    binding.nothingFound.visibility = View.GONE
                    binding.progressSearch.visibility = View.GONE
                    Toast.makeText(requireContext(), it.error, Toast.LENGTH_SHORT).show()
                }

                it.data?.let {

                    if (it == null) {
                        binding.nothingFound.visibility = View.VISIBLE
                    }
                    binding.progressSearch.visibility = View.GONE
                    latestCurrencyRate = it
                    currencyConversion()
                }
            }
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("Not yet implemented")
    }

    override fun onDestroy() {
        super.onDestroy()

    }


}