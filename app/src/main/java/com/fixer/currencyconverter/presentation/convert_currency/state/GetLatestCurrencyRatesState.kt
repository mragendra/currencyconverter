package com.fixer.currencyconverter.presentation.convert_currency.state

import com.fixer.currencyconverter.domain.model.LatestCurrencyRate

data class GetLatestCurrencyRatesState(
    val isLoading: Boolean = false,
    val data: LatestCurrencyRate? = null,
    val error: String = ""
)
