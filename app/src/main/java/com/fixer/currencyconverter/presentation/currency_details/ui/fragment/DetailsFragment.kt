package com.fixer.currencyconverter.presentation.currency_details.ui.fragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.coroutineScope
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.fixer.currencyconverter.presentation.currency_details.ui.viewmodel.DetailsViewModel
import com.fixer.currencyconverter.common.Constant
import com.fixer.currencyconverter.databinding.FragmentDetailsBinding
import com.fixer.currencyconverter.domain.model.CurrencyDetail
import com.fixer.currencyconverter.domain.model.LatestCurrencyRate
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

@AndroidEntryPoint
class DetailsFragment : Fragment() {

    private var _binding: FragmentDetailsBinding? = null
    val binding: FragmentDetailsBinding
        get() = _binding!!

    private val viewModel: DetailsViewModel by viewModels()

    private val args: DetailsFragmentArgs by navArgs()
    private val currencyHistoryList = mutableListOf<CurrencyDetail>()
    private val popularCurrencyDetailList = mutableListOf<CurrencyDetail>()
    private var popularCurrencyList =
        listOf("INR", "GBP", "NZD", "QAR", "BTC", "BHD", "USD", "JPY", "IQD", "RUB")

    private var count = 1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDetailsBinding.inflate(inflater, container, false)
        return _binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.historyRV.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = CurrencyHistoryAdapter(currencyHistoryList)
        }

        binding.popularCurrencyRV.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = CurrencyHistoryAdapter(popularCurrencyDetailList)
        }

        callLatestCurrencyRateApi()

    }

    private fun callHistoryApi(index: Int) {
//        if (count > 3) return
        var option: HashMap<String, String> = HashMap()
        option["access_key"] = Constant.FIXER_API_KEY
        option["base"] = Constant.BASE_CURRENCY
        option["symbols"] = "${args.fromCurrency},${args.toCurrency}"

        var reqdate: String = getCalculatedDate(index);
        viewModel.getHistoricalCurrencyRate(reqdate, option)
        Log.v("callHistory", ""+count++)
        lifecycle.coroutineScope.launch {
            viewModel.currencyRateList.collect {
                if (it.isLoading) {
                    binding.nothingFound.visibility = View.GONE
                    binding.progressSearch.visibility = View.VISIBLE
                }
                if (it.error.isNotBlank()) {
                    binding.nothingFound.visibility = View.GONE
                    binding.progressSearch.visibility = View.GONE
                    Toast.makeText(requireContext(), it.error, Toast.LENGTH_SHORT).show()
                }

                it.data?.let {

                    if (it == null) {
                        binding.nothingFound.visibility = View.VISIBLE
                    }
                    binding.progressSearch.visibility = View.GONE
                    showCurrencyDetails(it, reqdate)
                }
            }
        }
    }

    private fun callLatestCurrencyRateApi() {
        var option: MutableMap<String, String> = HashMap()
        option["access_key"] = Constant.FIXER_API_KEY
        option["base"] = Constant.BASE_CURRENCY
        option["symbols"] = "${args.fromCurrency},${popularCurrencyList.joinToString()}"

        viewModel.getLatestCurrencyRates(option)
        lifecycle.coroutineScope.launchWhenCreated {
            viewModel.latestCurrencyRateList.collect {
                if (it.isLoading) {
                    binding.nothingFound.visibility = View.GONE
                    binding.progressSearch.visibility = View.VISIBLE
                }
                if (it.error.isNotBlank()) {
                    binding.nothingFound.visibility = View.GONE
                    binding.progressSearch.visibility = View.GONE
                    Toast.makeText(requireContext(), it.error, Toast.LENGTH_SHORT).show()
                }

                it.data?.let {

                    if (it == null) {
                        binding.nothingFound.visibility = View.VISIBLE
                    }
                    binding.progressSearch.visibility = View.GONE
                    showPopularCurrencyConversionDetail(it)
                }
            }
        }
    }

    private fun showPopularCurrencyConversionDetail(latestCurrencyRate: LatestCurrencyRate) {
        val jsonString = Gson().toJson(latestCurrencyRate.rates)
        var jsonObject = JSONObject(jsonString)
        val fromCurrencyRate = jsonObject.getString(args.fromCurrency).toDouble()
        var oneOfFromCurrency = 1 / fromCurrencyRate
        for (index in popularCurrencyList.indices) {
            val toCurrencyRate = jsonObject.getString(popularCurrencyList[index]).toDouble()
            var oneOfToCurrency = String.format("%.3f", oneOfFromCurrency * toCurrencyRate)
            var currencyDetail = CurrencyDetail(
                "",
                "Name: ${popularCurrencyList[index]} value: $oneOfToCurrency",
                ""
            )
            popularCurrencyDetailList.add(currencyDetail)
        }

        for (index in 1..3) {
            callHistoryApi(index)
        }
    }

    private fun showCurrencyDetails(
        latestCurrencyRate: LatestCurrencyRate,
        reqdate: String,
    ) {
        var fromCurrency = args.fromCurrency
        var toCurrency = args.toCurrency

        val jsonString = Gson().toJson(latestCurrencyRate.rates)
        var jsonObject = JSONObject(jsonString)
        val fromCurrencyRate = jsonObject.getString(fromCurrency).toDouble()
        val toCurrencyRate = jsonObject.getString(toCurrency).toDouble()
        var currencyDetail = CurrencyDetail(
            "Date: $reqdate",
            "$fromCurrency: $fromCurrencyRate",
            "$toCurrency: $toCurrencyRate"
        )
        if (currencyHistoryList.size < 3)
            currencyHistoryList.add(currencyDetail)
    }

    private fun getCalculatedDate(days: Int): String {
        val negativeDays: Int = (days - 1).inv()
        val cal = Calendar.getInstance()
        val s = SimpleDateFormat(Constant.DATE_FORMAT)
        cal.add(Calendar.DAY_OF_YEAR, negativeDays)
        return s.format(Date(cal.timeInMillis))
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}