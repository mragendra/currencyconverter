package com.fixer.currencyconverter.presentation.convert_currency.state

data class ConvertCurrencyState(
    val isLoading: Boolean = false,
    val data: List<String>? = null,
    val error: String = ""
)