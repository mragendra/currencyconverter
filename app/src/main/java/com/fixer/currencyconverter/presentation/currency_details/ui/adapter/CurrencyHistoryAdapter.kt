package com.fixer.currencyconverter.presentation.currency_details.ui.fragment

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.fixer.currencyconverter.R
import com.fixer.currencyconverter.domain.model.CurrencyDetail

class CurrencyHistoryAdapter (private val list: List<CurrencyDetail>)
    : RecyclerView.Adapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val detail: CurrencyDetail = list[position]
        holder.bind(detail)
    }

    override fun getItemCount(): Int = list.size

}

class ViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.list_item, parent, false)) {
    private var mDate: TextView? = null
    private var mCurr1: TextView? = null
    private var mCurr2: TextView? = null


    init {
        mDate = itemView.findViewById(R.id.date)
        mCurr1 = itemView.findViewById(R.id.curr1)
        mCurr2 = itemView.findViewById(R.id.curr2)
    }

    fun bind(detail: CurrencyDetail) {
        mDate?.text = detail.date
        mCurr1?.text = detail.currency1Value
        mCurr2?.text = detail.currency2Value
    }

}
