package com.fixer.currencyconverter.common

object Constant {
    const val FIXER_API_KEY = "873a3289ff9d5e37d55ba659ec71e48a"
    const val BASE_URL = "http://data.fixer.io/api/"
    const val BASE_CURRENCY = "EUR"
    const val DATE_FORMAT = "yyyy-MM-dd"
}
