package com.fixer.currencyconverter.presentation.convert_currency.ui.fragment

import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.Lifecycle
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.fixer.currencyconverter.R
import org.hamcrest.CoreMatchers.`is`
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class ConvertCurrencyFragmentTest {

    private lateinit var scenario: FragmentScenario<ConvertCurrencyFragment>

    @Before
    fun setup() {
        scenario = launchFragmentInContainer(themeResId = R.style.Theme_FixerCurrencyConverter)
        scenario.moveToState(Lifecycle.State.STARTED)
    }

    @Test
    fun testFromSpinnerClick() {
        val currencyArray: Array<String> = arrayOf("EGP", "USD", "INR", "EUR")
        val size: Int = currencyArray.size
        for (i in 0 until size) {
            onView(withId(R.id.from_spinner)).perform(click())
            onData(`is`(currencyArray[i])).perform(click())
        }
    }

    @Test
    fun testToSpinnerClick() {
        val currencyArray: Array<String> = arrayOf("EGP", "USD", "INR", "EUR")
        val size: Int = currencyArray.size
        for (i in 0 until size) {
            onView(withId(R.id.to_spinner)).perform(click())
            onData(`is`(currencyArray[i])).perform(click())
        }
    }

}